---
title: "De standaard"
path: "/docs/de-standaard"
---

Haven is een standaard voor cloud-agnostische infrastructuur en schrijft een specifieke configuratie van [Kubernetes](https://www.kubernetes.io) voor om het geschikt te maken voor gebruik binnen overheden.

Voorbeelden van specifieke configuratie zijn:

- Het cluster voldoet aan de [CNCF Conformance test](https://github.com/cncf/k8s-conformance#certified-kubernetes).
- Er worden centraal metrics verzameld waarmee de gezondheid van een cluster gemonitord kan worden en capaciteitsplanning uitgevoerd kan worden.
- De control plane van het cluster is High Available uitgevoerd.
- Worker nodes zijn uitgevoerd met SELinux, Grsecurity, AppArmor of LKRG.

Deze lijst is niet volledig. De Haven Compliancy Checker beschrijft namelijk alle onderdelen waaraan een cluster moet voldoen. De [Haven website](/standard/) geeft een identiek overzicht.


## Haven Compliancy Checker

_De [Haven Compliancy Checker](/docs/compliancy-checker) borgt de standaard door geautomatiseerd clusters te valideren._ Dit is de essentie van Haven en de validatie staat op zichzelf.

Gebruik van een [Referentie Implementatie](https://gitlab.com/commonground/haven/haven/-/tree/master/reference) of [Addons](/docs/addons) is dus niet verplicht en heeft geen invloed op Haven Compliancy.

Er zijn verschillende implementaties van leveranciers en cloudproviders waarvan we hebben vastgesteld dat ze Haven Compliant zijn. Bekijk voor meer informatie de [aan de slag](/docs/aan-de-slag) pagina.

## Beheer van de standaard

De Haven standaard is zoals iedere standaard onderhevig aan wijzigingen. Ontwikkelingen van (cloud) providers, leveranciers en gebruikte technologie volgen elkaar immers in hoog tempo op. Uitgangspunt is balans te houden tussen nut en noodzaak van updates en eventuele druk op beheer.

Een voorstel tot het wijzigen, toevoegen of verwijderen van een check in de Haven Compliancy Checker - en daarmee de Haven Standaard - kan als volgt worden ingediend:

- Open een Haven [Gitlab issue](https://gitlab.com/commonground/haven/haven/-/issues/new?issue) met label "Standard change". Deze issue is publiek inzichtelijk.
- De community is vervolgens uitgenodigd om te reageren op het voorstel in de issue.
- Het Haven kern team besluit op basis van de input van de community of het voorstel geaccepteerd of geweigerd wordt.
- Volgens het Haven release management zullen eventuele wijzigingen vervolgens worden meegenomen naar nieuwe versies van Haven.

Een voorbeeld: Haven vereist dat een Kubernetes cluster wordt voorzien van een logging voorziening. Hiertoe is een _whitelist_ opgesteld van bekende toepassingen die hierin kunnen voorzien. Indien er een nieuwe toepassing in beeld komt die ook in logging kan voorzien, kan deze worden opgenomen in de whitelist van de bewuste check.

## Release management

### Versionering

Haven werkt met Major.Minor.Patch nummerieke versionering. Een voorbeeld: v9.1.3.

Wijzigingen aan de standaard die mogelijk impact hebben op beheer brengen automatisch een ophoging van de Major versie met zich mee. Een voorbeeld: een nieuw te installeren onderdeel in het cluster is vereist.

Wijzigingen die geen impact hebben op beheer zullen middels Minor en Patch versies worden uitgegeven. Denk aan het uitbreiden van een whitelist, het toevoegen van een Referentie Implementatie of het aanpassen van documentatie.

### Releases

Haven streeft er naar om nieuwe Major releases per kwartaal uit te brengen, dat wil zeggen januari, april, juli en oktober. Minor en Patch versies kunnen doorlopend worden uitgegeven.

Advies is derhalve om een Haven omgeving minimaal vier keer per jaar op Haven Compliancy te checken, of doorlopend.

Wanneer een omgeving reeds Haven Compliant is en een nieuwe Major versie van Haven wordt uitgebracht, kan het voorkomen dat de Haven Compliancy Checker aangeeft dat de bestaande omgeving niet meer Haven Compliant is.

In voorkomend geval is er een tijdsvak van 3 maanden om de nodige wijzigingen door te voeren aan de bestaande omgeving en deze opnieuw Haven Compliant te maken.

&ensp;

---

*Tijd om [aan de slag](/docs/aan-de-slag) te gaan!*
