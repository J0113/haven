import PropTypes from 'prop-types'
import React from 'react'

import Logo from '../Logo'
import { StyledHeader, StyledContainer, StyledLogoLink } from './index.styles'

const Header = ({ siteTitle }) => (
  <StyledHeader>
    <StyledContainer>
      <StyledLogoLink to="/">
        <Logo width="40px" height="40px" />
        <span>{siteTitle}</span>
      </StyledLogoLink>
    </StyledContainer>
  </StyledHeader>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: `Haven`,
}

export default Header
