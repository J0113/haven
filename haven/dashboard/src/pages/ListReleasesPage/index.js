// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string, object } from 'prop-types'
import { Route, Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Alert, Button } from '@commonground/design-system'
import PageTemplate from '../../components/PageTemplate'
import EmptyContentMessage from '../../components/EmptyContentMessage'
import LoadingMessage from '../../components/LoadingMessage'
import Table from '../../components/Table'
import ReleaseDetailPage from '../ReleaseDetailPage'
import usePromise from '../../hooks/use-promise'
import useInterval from '../../hooks/use-interval'
import services from '../../services'
import Status from './Status'
import {
  StyledActionsBar,
  StyledIconPlus,
  StyledCount,
  StyledAmount,
} from './index.styles'

const ReleaseRow = ({ metadata, status }) => (
  <Table.Tr
    to={`/releases/${metadata.namespace}/${metadata.name}`}
    name={metadata.name}
  >
    <Table.Td>{metadata.name}</Table.Td>
    <Table.Td>{metadata.namespace}</Table.Td>
    <Table.Td>{status ? <Status status={status} /> : null}</Table.Td>
  </Table.Tr>
)

ReleaseRow.propTypes = {
  metadata: object,
  status: string,
}

const ListReleasesPage = () => {
  const { t } = useTranslation()
  const { isReady, error, result, reload } = usePromise(
    services.helmReleases.list,
  )

  useInterval(() => {
    reload()
  }, 2000)

  return (
    <PageTemplate>
      <PageTemplate.Header title={t('Releases')} />

      <StyledActionsBar>
        <StyledCount>
          <StyledAmount>{result ? result.items.length : 0}</StyledAmount>
          <small>{t('Releases')}</small>
        </StyledCount>
        <Button as={Link} to="/releases/add" aria-label={t('Add release')}>
          <StyledIconPlus />
          {t('Add release')}
        </Button>
      </StyledActionsBar>

      {!isReady ? (
        <LoadingMessage />
      ) : error ? (
        <Alert variant="error">{t('Failed to load releases.')}</Alert>
      ) : result != null && result.items.length === 0 ? (
        <EmptyContentMessage>
          {t('There are no releases yet.')}
        </EmptyContentMessage>
      ) : result ? (
        <Table withLinks role="grid">
          <thead>
            <Table.TrHead>
              <Table.Th>{t('Name')}</Table.Th>
              <Table.Th>{t('Namespace')}</Table.Th>
              <Table.Th>{t('Status')}</Table.Th>
            </Table.TrHead>
          </thead>
          <tbody>
            {result.items.map((release, i) => (
              <ReleaseRow key={i} {...release} />
            ))}
          </tbody>
        </Table>
      ) : null}
      <Route path="/releases/:namespace/:name">
        <ReleaseDetailPage parentUrl="/releases" refreshHandler={reload} />
      </Route>
    </PageTemplate>
  )
}

export default ListReleasesPage
