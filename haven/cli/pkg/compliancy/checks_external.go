// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"bytes"
	"context"
	"encoding/base64"
	"fmt"
	"io"
	"os"
	"os/exec"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"time"

	batchv1 "k8s.io/api/batch/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func externalCNCF(config *Config) (Result, error) {
	logging.Info("Check CNCF: Running compliancy external CNCF checker. This usually takes about 1.5 hours. See: https://github.com/vmware-tanzu/sonobuoy.\n")

	run := "sonobuoy run --mode=certified-conformance"

	if config.HostPlatform == PlatformOpenShift {
		run += " --dns-namespace=openshift-dns --dns-pod-labels=dns.operator.openshift.io/daemonset-dns=default"
	}

	// Run.
	cmd := strings.Fields(run)
	runner := exec.Command(cmd[0], cmd[1:]...)
	out, err := runner.CombinedOutput()

	if err != nil {
		if !strings.Contains(string(out), "namespace already exists") {
			return ResultNo, fmt.Errorf("\n\ncommand: `%s`\n\nstdout:\n%s\nstderr:\n%s\n", strings.Join(cmd, " "), out, err)
		}

		logging.Info("Check CNCF: Sonobuoy namespace already exists: resuming check.\n")
	}

	// Intercept.
	ctrlc := false

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT)

	go func() {
		<-sigs

		logging.Warning("\n\n** User break: aborting **\n\n")

		ctrlc = true
	}()

	// Query.
	cmd = strings.Fields("sonobuoy retrieve")

	for {
		if ctrlc {
			break
		}

		runner = exec.Command(cmd[0], cmd[1:]...)
		out, err = runner.CombinedOutput()

		if err == nil {
			break
		}

		seconds := 0
		for {
			if ctrlc {
				break
			}

			seconds += 1

			if 1 == seconds {
				logging.Info("Check CNCF: Sleeping for 10 minutes before checking progress again.\n")
			}

			if 600 == seconds {
				seconds = 0

				break
			}

			time.Sleep(time.Second)
		}
	}

	results := "Status: failed"

	// Process.
	if !ctrlc {
		outputfile := string(out)

		cmd = strings.Fields(fmt.Sprintf("sonobuoy results %s", outputfile))
		runner = exec.Command(cmd[0], cmd[1:]...)
		out, err = runner.CombinedOutput()

		if err != nil {
			return ResultNo, fmt.Errorf("\n\ncommand: `%s`\n\nstdout:\n%s\nstderr:\n%s\n", strings.Join(cmd, " "), out, err)
		}

		results = string(out)
	}

	// Cleanup.
	logging.Info("Check CNCF: Cleaning up sonobuoy deployment.\n")

	cmd = strings.Fields("sonobuoy delete --all")
	runner = exec.Command(cmd[0], cmd[1:]...)
	out, err = runner.CombinedOutput()

	if err != nil {
		return ResultNo, fmt.Errorf("\n\ncommand: `%s`\n\nstdout:\n%s\nstderr:\n%s\n", strings.Join(cmd, " "), out, err)
	}

	if !strings.Contains(results, "Status: failed") {
		return ResultYes, nil
	}

	return ResultNo, nil
}

// suggestionExternalCIS is a 'Suggested Check' meaning it's not counted towards Haven Compliancy.
// Automating the manual CIS Benchmark is done via kube-bench. There are many ways to run kube-bench.
// - Most importantly it should work on any Haven cluster.
// - Tested successfully on Kops/OpenStack, GKE, AKS and EKS.
func suggestionExternalCIS(config *Config) (Result, error) {
	logging.Info("Check CIS: Running suggested external CIS checker on the worker nodes. This should not take long. See: https://github.com/aquasecurity/kube-bench.\n")

	app := "kube-bench-" + strings.ToLower(base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(time.Now().Nanosecond())))[:5])
	ns := "default"

	if _, err := config.KubeClient.BatchV1().Jobs(ns).Create(
		context.Background(),
		&batchv1.Job{
			ObjectMeta: metav1.ObjectMeta{
				Name: app,
			},
			Spec: batchv1.JobSpec{
				Template: apiv1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app": app,
						},
					},
					Spec: apiv1.PodSpec{
						HostPID:       true,
						RestartPolicy: apiv1.RestartPolicyNever,
						Containers: []apiv1.Container{
							{
								Name:    app,
								Image:   "aquasec/kube-bench:latest",
								Command: []string{"kube-bench", "node", "--version", fmt.Sprintf("%d.%d", config.KubeServer.Major(), config.KubeServer.Minor())},
								VolumeMounts: []apiv1.VolumeMount{
									{
										Name:      "var-lib-kubelet",
										MountPath: "/var/lib/kubelet",
										ReadOnly:  true,
									},
									{
										Name:      "etc-systemd",
										MountPath: "/etc/systemd",
										ReadOnly:  true,
									},
									{
										Name:      "etc-kubernetes",
										MountPath: "/etc-kubernetes",
										ReadOnly:  true,
									},
								},
							},
						},
						Volumes: []apiv1.Volume{
							{
								Name: "var-lib-kubelet",
								VolumeSource: apiv1.VolumeSource{
									HostPath: &apiv1.HostPathVolumeSource{
										Path: "/var/lib/kubelet",
									},
								},
							},
							{
								Name: "etc-systemd",
								VolumeSource: apiv1.VolumeSource{
									HostPath: &apiv1.HostPathVolumeSource{
										Path: "/etc/systemd",
									},
								},
							},
							{
								Name: "etc-kubernetes",
								VolumeSource: apiv1.VolumeSource{
									HostPath: &apiv1.HostPathVolumeSource{
										Path: "/etc/kubernetes",
									},
								},
							},
						},
					},
				},
			},
		},
		metav1.CreateOptions{},
	); err != nil {
		return ResultNo, err
	}

	defer func() {
		bg := metav1.DeletePropagationBackground

		if err := config.KubeClient.BatchV1().Jobs(ns).Delete(context.Background(), app, metav1.DeleteOptions{PropagationPolicy: &bg}); err != nil {
			logging.Error("Delete Job '%s': %s", app, err)
		}
	}()

	// Check Pod status max 1 minute.
	start := time.Now()
	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()

	for ts := range ticker.C {
		if ts.Sub(start).Seconds() >= 60 {
			return ResultNo, nil
		}

		pods, err := config.KubeClient.CoreV1().Pods(ns).List(context.Background(), metav1.ListOptions{LabelSelector: "job-name=" + app})
		if err != nil {
			return ResultNo, err
		}

		if len(pods.Items) != 1 {
			continue
		}

		// When ready check the results in Pod logs.
		if pods.Items[0].Status.Phase == "Succeeded" {
			plo := apiv1.PodLogOptions{}
			logs := config.KubeClient.CoreV1().Pods(ns).GetLogs(pods.Items[0].ObjectMeta.Name, &plo)

			stream, err := logs.Stream(context.Background())
			if err != nil {
				return ResultNo, err
			}

			buf := new(bytes.Buffer)
			_, err = io.Copy(buf, stream)

			if err := stream.Close(); err != nil {
				return ResultNo, err
			}

			if err != nil {
				return ResultNo, err
			}

			output := buf.String()

			logging.Output(output, "CIS")

			if strings.Contains(output, "0 checks FAIL") {
				return ResultYes, nil
			}

			break
		}
	}

	return ResultNo, nil
}
