// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package compliancy

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
)

type CompliancyOutput struct {
	Version        string
	HavenCompliant bool
	StartTS        time.Time
	StopTS         time.Time
	Config         struct {
		CNCF bool
		CIS  bool
	}
	CompliancyChecks struct {
		Results []Check
		Summary struct {
			Total   int
			Unknown int
			Skipped int
			Failed  int
			Passed  int
		}
	}
	SuggestedChecks struct {
		Results []Check
	}
}

type RationaleOutput struct {
	Version          string
	CompliancyChecks []Check
	SuggestedChecks  []Check
}

// outputJson returns a JSON object as a string.
func outputJson(output interface{}) (string, error) {
	b, err := json.Marshal(output)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s\n", string(b)), nil
}

// persistInCluster writes the Compliancy output to the cluster using the Haven CRD.
func persistInCluster(kubeConfig *rest.Config, compliant bool, output string) error {
	c, err := dynamic.NewForConfig(kubeConfig)
	if err != nil {
		return fmt.Errorf("Could not create dynamic Kubernetes client: %s", err.Error())
	}

	havenresource := &unstructured.Unstructured{
		Object: map[string]interface{}{
			"apiVersion": "haven.commonground.nl/v1alpha1",
			"kind":       "Compliancy",
			"metadata": map[string]interface{}{
				"name": fmt.Sprintf("haven-%s", time.Now().UTC().Format("20060102-150405")),
			},
			"spec": map[string]interface{}{
				"created":   time.Now().UTC(),
				"version":   fmt.Sprintf("Haven %s", Version),
				"compliant": compliant,
				"output":    output,
			},
		},
	}

	_, err = c.Resource(schema.GroupVersionResource{Group: "haven.commonground.nl", Version: "v1alpha1", Resource: "compliancies"}).Create(context.Background(), havenresource, metav1.CreateOptions{})
	if err != nil {
		return fmt.Errorf("Could not persist custom resource: %s", err.Error())
	}

	return nil
}
