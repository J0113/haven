// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package addons

import (
	"fmt"

	cli "github.com/jawher/mow.cli"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/helm"
	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

func cmdUpgrade(cmd *cli.Cmd) {
	name := cmd.StringArg("NAME", "", "The addon to install")

	cmd.Action = func() {
		addon, err := Get(*name)
		if err != nil {
			logging.Error("Error while getting addon: %s\n", err)
			return
		}

		helmClient, err := helm.NewClient(addon.Namespace)
		if err != nil {
			logging.Error("Error constructing client: %s\n", err)
		}

		logging.Info("Upgrading %s\n", addon.Name)

		_, err = helmClient.Upgrade(
			addon.Name,
			fmt.Sprintf("%s/%s", addon.Chart.Repository.Name, addon.Chart.Name),
			nil,
		)

		if err != nil {
			logging.Error("Error upgrading release: %s\n", err)
			return
		}

		logging.Info("Upgraded addon %s in namespace %s", addon.Name, addon.Namespace)
	}
}
