// Copyright © VNG Realisatie 2019-2021
// Licensed under EUPL v1.2

package connect

import (
	"os"
	"path/filepath"

	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/logging"
)

// K8s attempts to connect to a Kubernetes cluster via kube config or
// in cluster.
func K8s() (*rest.Config, error) {
	var cfg *rest.Config
	var err error
	var path string

	path = os.Getenv("KUBECONFIG")
	if "" == path {
		var home = os.Getenv("HOME")
		if "" == home {
			home = os.Getenv("USERPROFILE")
		}

		path = filepath.Join(home, ".kube", "config")
	}

	if _, err = os.Stat(path); err == nil {
		cfg, err = clientcmd.BuildConfigFromFlags("", path)
		if err != nil {
			logging.Error("Error while parsing KUBECONFIG: %s.\n", err)
			return nil, err
		}

		logging.Info("Kubernetes connection using KUBECONFIG: %s.\n", path)
	}

	if nil == cfg {
		cfg, err = rest.InClusterConfig()
		if err != nil {
			return nil, err
		}

		logging.Info("Kubernetes connection using In Cluster config.\n")
	}

	return cfg, nil
}
